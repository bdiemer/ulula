===================================================================================================
Simulation framework
===================================================================================================

This page documents the Ulula hydro solver and the implemented algorithms.

---------------------------------------------------------------------------------------------------
Overview
---------------------------------------------------------------------------------------------------

The Ulula simulation framework is basically implemented within a single class, called
:class:`~ulula.core.simulation.Simulation`. The :class:`~ulula.core.simulation.HydroScheme` class contains 
information about the chosen algorithm. The user does not need to interact directly with the 
:class:`~ulula.core.simulation.Simulation` class because the simulation workflow is already implemented 
in the :func:`ulula.core.run.run` runtime function (see :doc:`run`). If this workflow is not general 
enough, however, the user can run a simulation by implementing and/or modifying the following 
steps:

* Create a :class:`~ulula.core.simulation.Simulation` object
* Optionally update the hydro scheme, equation of state, code units, gravity, and user-defined
  boundary conditions using the functions
  :func:`~ulula.core.simulation.Simulation.setHydroScheme`,
  :func:`~ulula.core.simulation.Simulation.setEquationOfState`,
  :func:`~ulula.core.simulation.Simulation.setCodeUnits`,
  :func:`~ulula.core.simulation.Simulation.setGravityMode`,
  :func:`~ulula.core.simulation.Simulation.setUserUpdateFunction`, and
  :func:`~ulula.core.simulation.Simulation.setUserBoundaryConditions`.
* Execute the :func:`~ulula.core.simulation.Simulation.setDomain` function, which sets the domain in 
  pixels and physical coordinates and creates the variable arrays.  
* Set the initial conditions in primitive variables (into the simulation object's
  ``V`` array). This operation is implemented in each problem setup class (see :doc:`setups`).
  The primitive variable fields are listed below.
* If the physical setup contains gravity, execute 
  :func:`~ulula.core.simulation.Simulation.setGravityPotentials`.
* Execute the :func:`~ulula.core.simulation.Simulation.timestep` function until the simulation is 
  finished; create plots as desired.

The documentation of :class:`~ulula.core.simulation.Simulation` contains further details about the 
inner workings of the Ulula hydro solver. Ulula can save the current state of a simulation to an 
hdf5 file (see the :func:`~ulula.core.simulation.Simulation.save` function). The 
:func:`~ulula.core.simulation.load` function loads such a file and recovers a Simulation object 
identical to the one saved. The simulation can then be plotted or continued, meaning that the 
files serve as both snapshot and restart files.

---------------------------------------------------------------------------------------------------
Hydrodynamics background
---------------------------------------------------------------------------------------------------

The fundamental purpose of any hydrodynamics code is to solve the Euler equations. We
can understand these equations as a series of conservation laws, which we can most generally 
express as

.. math::
	\frac{\partial}{\partial t} (\mathrm{density\ of\ }Q) + \boldsymbol{\nabla} \boldsymbol{\cdot} (\mathrm{flux\ of\ }Q) = 0 \,.
	
In words, if the flux of a conserved quantity :math:`Q` (e.g., mass) diverges, the density of that
quantity decreases. If the flux converges (negative divergence), the density increases. The Euler
equations can be expressed as the following conservation laws for mass, momentum, and energy:

.. math::
	\begin{align}
	\frac{\partial \rho}{\partial t} + \boldsymbol{\nabla} \boldsymbol{\cdot} (\rho {\pmb u}) &= 0 \\
	\frac{\partial (\rho {\pmb u})}{\partial t} + \boldsymbol{\nabla} \boldsymbol{\cdot} (\rho {\pmb u} \otimes {\pmb u} + {\pmb I} P) &= -\rho \boldsymbol{\nabla} \Phi \\
	\frac{\partial E}{\partial t} + \boldsymbol{\nabla} \boldsymbol{\cdot} ([E + P] {\pmb u}) &= \rho \frac{\partial \Phi}{\partial t} \\
	\end{align}

where :math:`\rho` is density, :math:`{\pmb u}` the velocity vector, :math:`P` pressure, and
:math:`E` is the total energy, defined as 

.. math::
	E = \rho({\pmb u}^2 / 2 + \epsilon + \Phi) \,.

Here :math:`\epsilon` is the internal energy per unit mass. How this translates into temperature
and pressure is governed by the microscopic particle properties of the gas, which are summarized in
an equation of state. In Ulula, we always assume an ideal gas equation of state,

.. math::
	P = \rho \epsilon (\gamma - 1)
	
where :math:`\gamma` is typically :math:`5/3` for an atomic gas but can be changed by the user. The
non-zero right-hand-side of the conservation laws above means that some quantities are not always
conserved. In particular, in the presence of a gravitational potential :math:`\Phi`, momentum and
energy are added to the gas due to gravity.
In terms of how the equations are solved in Ulula (and most other finite-volume hydro codes), it
is easier to return to the abstract form of a conservation law above and write the three 
conservation laws as a single vector equation,

.. math::
	\frac{\partial {\pmb U}}{\partial t} + \boldsymbol{\nabla} \boldsymbol{\cdot} (\mathcal{F}({\pmb U})) = S({\pmb U}) \,,
	
where :math:`{\pmb U}` is the vector of conserved quantities (in 2D, in our case), 
:math:`\mathcal{F}` is the flux vector, and :math:`S` the source vector,

.. math::
	{\pmb U} \equiv
	\left(\begin{array}{c}
	\rho \\
	\rho u_{\rm x} \\
	\rho u_{\rm y} \\
	E
	\end{array}\right)
	\qquad
	\mathcal{F}({\pmb U})  \equiv
	\left(\begin{array}{c}
	\rho \pmb{u} \\
	\rho u_{\rm x} {\pmb u} + \delta_{xj} P \\
	\rho u_{\rm y} {\pmb u} + \delta_{yj} P \\
	(E + P) {\pmb u}
	\end{array}\right) 
	\qquad
	{\pmb S} \equiv
	\left(\begin{array}{c}
	0 \\
	- \rho\ \partial \Phi / \partial x \\
	- \rho\ \partial \Phi / \partial y \\
	\rho\ \partial \Phi / \partial t
	\end{array}\right) 

The Kronecker :math:`\delta` means that :math:`P` is only added to the x-component of the flux 
vector for :math:`u_{\rm x}` and so on. The basic MO of a code such as Ulula is to update the 
conserved quantities :math:`{\pmb U}` by estimating the time-integrated fluxes 
:math:`\mathcal{F}({\pmb U})` and taking their differences across cells. As shown by the equations 
above, in the process of computing those updates from the flux and source vectors we do need to 
know pressure, velocity and so on as well as the conserved variables. We thus frequently convert 
to and from so-called primitive variables,

.. math::
	{\pmb V} \equiv
	\left(\begin{array}{c}
	\rho \\
	u_{\rm x} \\
	u_{\rm y} \\
	P
	\end{array}\right) 

by using the definition of total energy and the equation of state (see above). All told, we need
to keep track of quite a few quantities in Ulula. The following table shows these fluid variables and 
abbreviations used throughout the code, as well as their units in terms of length, time, and mass:

==================================  ============  =============================================  ==========================
Symbol                              Abbreviation  Quantity                                       Units
==================================  ============  =============================================  ==========================
Primitive variables
---------------------------------------------------------------------------------------------------------------------------
:math:`\rho`                        ``DN``        Density                                        :math:`m/l^3`
:math:`u_{\rm x}`                   ``VX``        X-velocity                                     :math:`l/t`
:math:`u_{\rm y}`                   ``VY``        Y-velocity                                     :math:`l/t`
:math:`P`                           ``PR``        Pressure                                       :math:`m/l/t^2`
----------------------------------  ------------  ---------------------------------------------  --------------------------
Conserved variables
---------------------------------------------------------------------------------------------------------------------------
:math:`\rho`                        ``MS``        Mass density (same as density)                 :math:`m/l^3`
:math:`\rho u_{\rm x}`              ``MX``        X-momentum density                             :math:`m/l^2/t`
:math:`\rho u_{\rm y}`              ``MY``        Y-momentum density                             :math:`m/l^2/t`
:math:`E`                           ``ET``        Total energy density                           :math:`m/l/t^2`
----------------------------------  ------------  ---------------------------------------------  --------------------------
Gravity
---------------------------------------------------------------------------------------------------------------------------
:math:`\Phi`                        ``GP``        Potential per unit mass                        :math:`l^2/t^2`
:math:`\partial \Phi / \partial x`  ``GX``        Potential gradient in the X-direction          :math:`l/t^2`
:math:`\partial \Phi / \partial y`  ``GY``        Potential gradient in the Y-direction          :math:`l/t^2`
==================================  ============  =============================================  ==========================

The abbreviations are used to select variables in the :doc:`plots` module and when setting the
initial conditions. 

The equations solved by Ulula are invariant under unit transformations, meaning that the length,
mass, and time units that make up the composite units listed above can be set to arbitrary values,
so-called "code units" (using the :func:`~ulula.core.simulation.Simulation.setCodeUnits` 
function). Changing these units does not change the outcome of the simulation, but it will be 
reflected in plots (unless you choose to plot in code units). Code units can take on any value,
but some common units are listed in the :mod:`~ulula.physics.units` module.

---------------------------------------------------------------------------------------------------
Hydro schemes
---------------------------------------------------------------------------------------------------

The following tables list the algorithmic choices implemented in Ulula. Broadly speaking, Ulula 
uses a Godunov solver with either piecewise-constant or piecewise-linear state reconstruction and 
a either Euler or 2nd-order time integration (MUSCL-Hancock). 

.. list-table:: Spatial reconstruction schemes
   :widths: 20 80
   :header-rows: 1

   * - Identifier
     - Description
   * - ``const``
     - | Piecewise-constant; the scheme is 1st-order in space because the state within each cell is 
       | not interpolated. This hydro scheme tends to be highly diffusive and is impelemented for
       | test purposes.
   * - ``linear``
     - | Piecewise-linear; linear interpolation within each cell, the scheme is 2nd-order in space. 
       | The slope limiter decides how aggressive the reconstruction is in terms of accuracy vs. 
       | stability.

.. list-table:: Slope limiters
   :widths: 20 80
   :header-rows: 1

   * - Identifier
     - Description
   * - ``none``
     - | No limiter; results in a highly unstable scheme because the interpolation can lead to
       | negative values and other artifacts. Implemented for demonstration purposes only
   * - ``minmod``
     - | Minimum modulus; the most conservative limiter, takes the shallower of the left and right 
       | slopes
   * - ``vanleer``
     - The van Leer limiter; an intermediate between the minmod and mc limiters
   * - ``mc``
     - | Monotonized central; the most aggressive limiter, takes the central slope unless the slopes
       | are very different

.. list-table:: Riemann solvers
   :widths: 20 80
   :header-rows: 1

   * - Identifier
     - Description
   * - ``hll``
     - | The Harten-Lax-van Leer (HLL) Riemann solver; this simple algorithm considers only the 
       | fastest waves to the left and right and constructs an intermediate state, ignoring contact
       | discontinuities.
   * - ``hllc``
     - | The HLLC Riemann solver, which corresponds to HLL plus contact discontinuities. This 
       | solver cannot be used with an isothermal EOS, which makes sense since an isothermal gas
       | admits no contact discontinuities.

.. list-table:: Time integration schemes
   :widths: 20 80
   :header-rows: 1

   * - Identifier
     - Description
   * - ``euler``
     - | First-order time integration; the fluid state is advanced by a full timestep without 
       | any attempt to time-average the Godunov fluxes.
   * - ``hancock``
     - | Combined with linear reconstruction, this choice gives the MUSCL-Hancock scheme,  
       | where the states at the left and right cell edges are advanced by a half timestep. 
       | The change  in time is computed to linear order from the primitive-variable form 
       | of the Euler equations, applied to the left and right states separately. 
   * - ``hancock_cons``
     - | The same as ``hancock``, but using the conserved formulation where the fluxes 
       | corresponding to the left and right cell-edge states are computed and differenced  
       | to get an evolution in the fluid state. This formulation should be identical to 
       | the primitive formulation up to numerical errors, which provides a check of the 
       | algorithms.  The conservative version is slower due to a number of 
       | primitive-conserved conversions; thus, the primitive version is the default.

The purpose of Ulula is to experiment with hydro algorithms, including their failure. Thus, the
code allows sub-optimal (aka crazy) combinations of the parameters listed above. For
example, it allows setting no limiter or combining piecewise-constant states with a Hancock-style
time integration, which results in an unstable scheme. The user's algorithmic choices are 
contained in the HydroScheme class:

.. autoclass:: ulula.core.simulation.HydroScheme
    :members:
    
---------------------------------------------------------------------------------------------------
Simulation class
---------------------------------------------------------------------------------------------------

.. autoclass:: ulula.core.simulation.Simulation
    :members:

---------------------------------------------------------------------------------------------------
Input/Output
---------------------------------------------------------------------------------------------------

Files are saved using the :func:`~ulula.core.simulation.Simulation.save` function in a Simulation 
object. The file format is relatively self-explanatory. The attributes of the ``hydro_scheme`` 
group correspond to the parameters of the :class:`~ulula.core.simulation.HydroScheme` object. The
attributes of the ``domain``, ``physics``, and ``run`` groups have the same meaning as in the 
:class:`~ulula.core.simulation.Simulation` object. The ``code`` group contains the version of Ulula that
the filetype corresponds to. 

The ``grid`` group contains the 2D grid data for the given simulation in code units. The field 
names correspond to the abbreviations listed in the "Fluid quantities" section above. Only the 
physical domain is included (without ghost cells).

A file can be loaded into a new simulation object with the following function. It checks the 
file version against the current version of Ulula; if the file version is too old for the file to
be compatible, the loading process is aborted.

.. autofunction:: ulula.core.simulation.load
   