===================================================================================================
History & Contributions
===================================================================================================

---------------------------------------------------------------------------------------------------
Contributions
---------------------------------------------------------------------------------------------------

Ulula is developed and maintained by `Benedikt Diemer <https://www.benediktdiemer.com/>`_, 
University of Maryland. I am grateful for the following contributions:

* Code to create gif movies was contributed by `Calvin Osinga <https://www.calvinosinga.com/>`_.
* The computation of annulus-square overlaps in radial plots is based on code by 
  `Philip Mansfield <https://phil-mansfield.github.io/>`_.
* The exact solution to the Sedov-Taylor explosion problem was adapted from the 
  `Gandalf code <https://gandalfcode.github.io/>`_ by Hubber et al. 
  (`2018 <https://ui.adsabs.harvard.edu/abs/2018MNRAS.473.1603H/abstract>`_).
* Big thanks to all the graduate students at the University of Maryland who have experimented
  with Ulula, particularly Ell Bogat, Emeline Fromont, Giannina Guzman Caloca, and Calvin Osinga!

---------------------------------------------------------------------------------------------------
Version history
---------------------------------------------------------------------------------------------------

.. rubric:: Version 0.4.1 (released 3/5/2025)

* Added the HLLC Riemann solver and improved tests.
* Added Gresho vortex setup.
* Added 1D advection setup.
* Added the possibility of plotting temperature in Kelvin.
* True solution not plotted in 1D plots if not provided by setup.
* Simultaneous 1D and 2D plotting.
* Added radial and azimuthal velocity to plottable quantities.

.. rubric:: Version 0.4.0 (released 2/5/2025)

This is a major release. The most important changes are:

* Implemented proper 1D simulations.
* Added plotting of ghost cells in 1D.
* Added isothermal equation of state.
* Added user-defined changes at runtime and boundary conditions.
* Improved setting of simulation parameters.
* Removed global index variables in favor of adaptable indexing.
* Added atmosphere and soundwave setups.

.. rubric:: Version 0.3.0 (released 1/7/2025)

This is a major release. The most important changes are:

* Added gravity (fixed acceleration and fixed, user-defined potential).
* Added wall (reflective) boundary conditions.
* Added code units and plotting in arbitrary unit systems.
* Added correct treatment of CFL violations.
* Added automatic checks on conservation of mass, energy, and momentum (if applicable).
* Added cloud crushing, free-fall, Rayleigh-Taylor, and tidal disruption setups.

.. rubric:: Version 0.2.2 (released 05/20/2021)

Minor changes:

* Added a check that the final time must be larger when restarting
* Added to documentation

.. rubric:: Version 0.2.1 (released 05/19/2021)

The main runtime interface (see :doc:`run`) was reworked significantly:

* it now accommodates saving and plotting at evenly spaced times rather than snapshots
* saving, plotting, and movie making can be switched on at the same time
* the output and plotting times do not interfere with the physical simulation

.. rubric:: Version 0.2.0 (released 05/17/2021)

The main features added in this version relate to input/output of files:

* Added file saving into hdf5 snapshot files
* Added restarting capability (either manual or in runtime function)
* Added dictionaries of primitive and conserved fluid quantities for more transparency in the plotting
  and saving routines
* Added to documentation

.. rubric:: Version 0.1.1 (released 04/02/2021)

This version is mostly a refactoring of the initial checkin, and also contains:

* Added Sedov-Taylor test case (blastwave explosion)
* Added radially averaged 1D plots of fluid quantities

.. rubric:: Version 0.1.0 (released 03/24/2021)

Initial version. Contains the main hydro solver and examples.
