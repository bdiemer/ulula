===================================================================================================
Running Ulula
===================================================================================================

This page shows how Ulula can be run with a few lines of code and describes a few pre-implemented
example problems.

---------------------------------------------------------------------------------------------------
Quick start
---------------------------------------------------------------------------------------------------

The easiest way to execute Ulula is via the runtime function :func:`~ulula.core.run.run` (see 
documentation below). The main input to this function is a :class:`~ulula.core.setup_base.Setup`, a class that 
contains all data and routines that describe a particular physical problem. The following example 
runs a simulation of the Kelvin-Helmholtz test::

    import ulula.setups.kelvin_helmholtz as setup_kh
    import ulula.core.run as ulula_run

    setup = setup_kh.SetupKelvinHelmholtz()
    ulula_run.run(setup, tmax = 4.0, nx = 200)

This code will set up a domain with 200x200 cells and run the Kelvin-Helmholtz test until time 4.0
using the default hydro solver. If you want control over the algorithms used, the 
:class:`~ulula.core.simulation.HydroScheme` class contains all relevant settings, e.g.::

    import ulula.core.simulation as ulula_sim

    hs = ulula_sim.HydroScheme(reconstruction = 'linear', limiter = 'mc', cfl = 0.9)
    ulula_run.run(setup, hydro_scheme = hs, tmax = 4.0, nx = 200)

The :func:`~ulula.core.run.run` function takes a number of additional parameters, many of which govern
various possible output products such as interactive and saved figures as well as movies. For 
example, these code snippets would produce a series of density and pressure plots at time intervals 
of 0.5 or a movie of the evolution of density::

    ulula_run.run(setup, tmax = 4.0, nx = 200, plot_time = 0.5, q_plot = ['DN', 'PR'])
    ulula_run.run(setup, tmax = 4.0, nx = 200, movie = True, q_plot = ['DN'])

Internally, the :func:`~ulula.core.run.run` function creates and handles an Ulula 
:class:`~ulula.core.simulation.Simulation` object (see the :doc:`simulation` page for more details).
This simulation object can be saved to hdf5 files, either after regular time intervals or numbers
of snapshots::

    ulula_run.run(setup, tmax = 4.0, nx = 200, output_time = 0.5)
    ulula_run.run(setup, tmax = 4.0, nx = 200, output_step = 100)

Note that the various outputs (``plot_time``, ``plot_step``, ``output_time``, ``output_step``,
and ``movie``) can be used concurrently. Alternatively, the runtime function returns a simulation 
object, which can be saved manually::

    sim = ulula_run.run(setup, tmax = 2.0, nx = 200)
    sim.save(filename = 'my_file.hdf5')

Ulula files are both snapshots and restart files. For example, we can recreate a simulation 
object from a file and run it to a later time::

    ulula_run.run(setup, restart_file = 'my_file.hdf5', tmax = 4.0)

In this case, the data from the restart file overwrite all other input to the runtime function.
While the :func:`~ulula.core.run.run` function has plenty of options to create plots during a 
simulation run, we can also load a file and plot it. Note that the plot functions create a plot
but do not save or show it::

    import matplotlib.pyplot as plt
    import ulula.core.plots as ulula_plots
    
    sim = ulula_sim.load('my_file.hdf5')
    ulula_plots.plot2d(sim, q_plot = ['DN', 'PR'])
    plt.show()

For more information on the plotting routines and plottable fluid quantities, see :doc:`plots`. 

---------------------------------------------------------------------------------------------------
Runtime function
---------------------------------------------------------------------------------------------------

.. automodule:: ulula.core.run
    :members:
    
---------------------------------------------------------------------------------------------------
Examples
---------------------------------------------------------------------------------------------------

The following code samples are showcase how to run different problem setups, change hydro solvers, 
and produce plots and movies. The physical setups are described in detail in :doc:`setups`.

.. autosummary::
    ~ulula.examples.examples.runAdvection1D
    ~ulula.examples.examples.runSoundwave
    ~ulula.examples.examples.runShocktube
    ~ulula.examples.examples.runFreefall
    ~ulula.examples.examples.runAtmosphere
    ~ulula.examples.examples.runAdvection2D
    ~ulula.examples.examples.runKelvinHelmholtz
    ~ulula.examples.examples.runCloudCrushing
    ~ulula.examples.examples.runSedovTaylor
    ~ulula.examples.examples.runGreshoVortex
    ~ulula.examples.examples.runRayleighTaylor
    ~ulula.examples.examples.runTidalDisruption

.. automodule:: ulula.examples.examples
    :members:
    