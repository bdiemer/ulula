===================================================================================================
General setup class
===================================================================================================

.. autoclass:: ulula.core.setup_base.Setup
    :members:
