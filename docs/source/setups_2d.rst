===================================================================================================
2D setups
===================================================================================================

---------------------------------------------------------------------------------------------------
Advection (2D)
---------------------------------------------------------------------------------------------------

The following image shows the results of the advection test for different hydro algorithms:

.. image:: ../images/setup_advection_2d.png
   :align: center
   :scale: 40 %

|

.. autoclass:: ulula.setups.advection_2d.SetupAdvection2D
    :special-members: __init__

---------------------------------------------------------------------------------------------------
Kelvin-Helmholtz instability
---------------------------------------------------------------------------------------------------

The following images show the evolution of density in the Kelvin-Helmholtz setup:

.. image:: ../images/setup_kelvin_helmholtz.png
   :align: center
   :scale: 50 %

|
    
.. autoclass:: ulula.setups.kelvin_helmholtz.SetupKelvinHelmholtz
    :special-members: __init__

---------------------------------------------------------------------------------------------------
Cloud crushing
---------------------------------------------------------------------------------------------------

The following images show the evolution of the cloud crushing setup:

.. image:: ../images/setup_cloud_crushing.png
   :align: center
   :scale: 40 %

|

.. autoclass:: ulula.setups.cloud_crushing.SetupCloudCrushing
    :special-members: __init__

---------------------------------------------------------------------------------------------------
Sedov-Taylor explosion
---------------------------------------------------------------------------------------------------

The following images shows the Sedov-Taylor blastwave setup as the shock is about to reach the edge
of the domain, plotted both in 2D and 1D:

.. image:: ../images/setup_sedov_taylor.png
   :align: center
   :scale: 40 %

|

.. autoclass:: ulula.setups.sedov_taylor.SetupSedovTaylor
    :special-members: __init__

---------------------------------------------------------------------------------------------------
Gresho Vortex
---------------------------------------------------------------------------------------------------

The following images shows the Gresho vortex setup after a number of revolutions, plotted both in 
2D and 1D:

.. image:: ../images/setup_gresho_vortex.png
   :align: center
   :scale: 40 %

|

.. autoclass:: ulula.setups.gresho_vortex.SetupGreshoVortex
    :special-members: __init__

---------------------------------------------------------------------------------------------------
Rayleigh-Taylor instability
---------------------------------------------------------------------------------------------------
    
The following images show the evolution of the Rayleigh-Taylor instability setup:

.. image:: ../images/setup_rayleigh_taylor.png
   :align: center
   :scale: 28 %
    
|

.. autoclass:: ulula.setups.rayleigh_taylor.SetupRayleighTaylor
    :special-members: __init__

---------------------------------------------------------------------------------------------------
Tidal disruption
---------------------------------------------------------------------------------------------------

The following images show the gravitational potential and the density evolution of the tidal 
disruption setup:

.. image:: ../images/setup_tidal_disruption.png
   :align: center
   :scale: 30 %

|

.. autoclass:: ulula.setups.tidal_disruption.SetupTidalDisruption
    :special-members: __init__
    