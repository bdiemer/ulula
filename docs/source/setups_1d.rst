===================================================================================================
1D setups
===================================================================================================

---------------------------------------------------------------------------------------------------
Advection (1D)
---------------------------------------------------------------------------------------------------

The following image shows the results of the 1D advection test for different hydro algorithms:

.. image:: ../images/setup_advection_1d.png
   :align: center
   :scale: 40 %

|

.. autoclass:: ulula.setups.advection_1d.SetupAdvection1D
    :special-members: __init__

---------------------------------------------------------------------------------------------------
Soundwave
---------------------------------------------------------------------------------------------------

The image below shows the soundwave setup after a few wave periods:

.. image:: ../images/setup_soundwave.png
   :align: center
   :scale: 25 %

|

.. autoclass:: ulula.setups.soundwave.SetupSoundwave
    :special-members: __init__

---------------------------------------------------------------------------------------------------
Shocktube
---------------------------------------------------------------------------------------------------

The image below shows the shocktube test for the default hydro solver:

.. image:: ../images/setup_shocktube.png
   :align: center
   :scale: 22 %

|

.. autoclass:: ulula.setups.shocktube.SetupShocktube
    :special-members: __init__

---------------------------------------------------------------------------------------------------
Free-fall
---------------------------------------------------------------------------------------------------

The image below shows the free-fall test at the initial and two later times:

.. image:: ../images/setup_freefall.png
   :align: center
   :scale: 30 %
    
|

.. autoclass:: ulula.setups.freefall.SetupFreefall
    :special-members: __init__
   
---------------------------------------------------------------------------------------------------
Atmosphere
---------------------------------------------------------------------------------------------------

The image below shows the atmosphere setup at the initial time and after 10 hours:

.. image:: ../images/setup_atmosphere.png
   :align: center
   :scale: 30 %

|

.. autoclass:: ulula.setups.atmosphere.SetupAtmosphere
    :special-members: __init__
