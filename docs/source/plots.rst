===================================================================================================
Plotting
===================================================================================================

This page documents Ulula's plotting capabilities.

.. automodule:: ulula.core.plots

---------------------------------------------------------------------------------------------------
Overview
---------------------------------------------------------------------------------------------------

Ulula comes with two plotting routines for 1D and 2D data, :func:`~ulula.core.plots.plot1d` and 
:func:`~ulula.core.plots.plot2d`. Both plot multiple panels for multiple fluid quantities that are 
given as a list of strings. Valid identifiers are listed in the :data:`~ulula.core.plots.fields` 
dictionary. A simulation object must be passed to the plotting functions.

---------------------------------------------------------------------------------------------------
Plottable quantities
---------------------------------------------------------------------------------------------------

All plotting functions take a ``q_plot`` parameter that contains a list of the fluid variables to 
be plotted. The fields and their properties are defined in the following dictionary:

.. autodata:: fields
	:no-value:

Possible values include the primitive and conserved variables that the simulation keeps track of 
(see :doc:`simulation`), as well as some derived quantities:

============  ======
Abbreviation  Quantity
============  ======
``DN``        Density
``VX``        X-velocity
``VY``        Y-velocity
``VT``        Total velocity
``MX``        X-momentum
``MY``        Y-momentum
``PR``        Pressure
``ET``        Total energy (internal + kinetic + potentials)
``EI``        Internal energy
``TK``        Temperature in Kelvin
``GP``        Gravitational potential
``GX``        Gravitational potential gradient in x
``GY``        Gravitational potential gradient in y
============  ======

---------------------------------------------------------------------------------------------------
Units
---------------------------------------------------------------------------------------------------

.. automodule:: ulula.physics.units

The user can plot in code units or choose a unit system for plotting, which may or may not 
correspond to the system of code units set in the simulation itself. For example, if the unit 
system of the underlying simulation is cgs (the default), then plotting with unit
length, time, and mass of 1 centimeter, 1 second, and 1 gram leads to exactly the same plot as
plotting in code units (albeit with slightly different labels). However, instead choosing 1 meter
as the plotted unit will change the spatial axes as well as fluid properties that depend on length, 
such as density. In the plotting functions documented below, the plotted units are chosen via the
following string codes:

=============== ==================== ============================== 
Abbreviation    Symbol               CGS value                      
=============== ==================== ============================== 
Length (in cm)
-------------------------------------------------------------------
``nm``          :math:`{\rm nm}`     :math:`10^{-7}`                
``mum``         :math:`{\rm \mu m}`  :math:`10^{-4}`                
``mm``          :math:`{\rm mm}`     :math:`10^{-1}`                
``cm``          :math:`{\rm cm}`     :math:`1`                      
``m``           :math:`{\rm m}`      :math:`10^{2}`                 
``km``          :math:`{\rm km}`     :math:`10^{5}`                 
``au``          :math:`{\rm AU}`     :math:`1.496\times10^{13}`     
``pc``          :math:`{\rm pc}`     :math:`3.086\times10^{18}`     
``kpc``         :math:`{\rm kpc}`    :math:`3.086\times10^{21}`     
``Mpc``         :math:`{\rm Mpc}`    :math:`3.086\times10^{24}`     
``Gpc``         :math:`{\rm Gpc}`    :math:`3.086\times10^{27}`     
--------------- -------------------- ------------------------------ 
Time (in s)
-------------------------------------------------------------------
``ns``          :math:`{\rm ns}`     :math:`10^{-9}`                
``mus``         :math:`{\rm \mu s}`  :math:`10^{-6}`                
``ms``          :math:`{\rm ms}`     :math:`10^{-3}`                
``s``           :math:`{\rm s}`      :math:`1`                      
``min``         :math:`{\rm min}`    :math:`6.000\times10^{1}`      
``hr``          :math:`{\rm hr}`     :math:`3.600\times10^{3}`      
``yr``          :math:`{\rm yr}`     :math:`3.156\times10^{7}`      
``kyr``         :math:`{\rm kyr}`    :math:`3.156\times10^{10}`     
``Myr``         :math:`{\rm Myr}`    :math:`3.156\times10^{13}`     
``Gyr``         :math:`{\rm Gyr}`    :math:`3.156\times10^{16}`     
--------------- -------------------- ------------------------------ 
Mass (in g)
-------------------------------------------------------------------
``u``           :math:`{\rm u}`      :math:`1.661\times10^{-24}`    
``mp``          :math:`m_{\rm p}`    :math:`1.673\times10^{-24}`    
``ng``          :math:`{\rm ng}`     :math:`10^{-9}`                
``mug``         :math:`{\rm \mu g}`  :math:`10^{-6}`                
``mg``          :math:`{\rm mg}`     :math:`10^{-3}`                
``g``           :math:`{\rm g}`      :math:`1`                      
``kg``          :math:`{\rm kg}`     :math:`10^{3}`                 
``t``           :math:`{\rm t}`      :math:`10^{6}`                 
``Mear``        :math:`M_{\oplus}`   :math:`5.972\times10^{27}`     
``Msun``        :math:`M_{\odot}`    :math:`1.988\times10^{33}`     
=============== ==================== ============================== 

---------------------------------------------------------------------------------------------------
Plotting functions
---------------------------------------------------------------------------------------------------

.. autofunction:: ulula.core.plots.plot1d

.. autofunction:: ulula.core.plots.plot2d

---------------------------------------------------------------------------------------------------
Helper functions
---------------------------------------------------------------------------------------------------

If you wish to create an entirely different plot without using the functions above, the following
helper function might be helpful. 

.. autofunction:: ulula.core.plots.getPlotQuantities
